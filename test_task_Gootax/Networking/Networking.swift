import UIKit
import Alamofire

class Networking {
  // MARK: - property
  private let headers: HTTPHeaders = ["typeclient": "ios", "tenantId": "68"]

  // MARK: - request types
  enum RequestType {
    case urlListNews
    case urlAloneNews(idNews: String)
  }

  // MARK: - get news
  func getNetworkNews<T: Codable>(generalType: T.Type,
                                  requestType: RequestType,
                                  completion: @escaping (T) -> Void) {
    var urlString = ""
    switch requestType {
      case .urlListNews:
        urlString = "https://ca.uatgootax.ru/v1/get_news?current_time=423423322&city_id=26068"
      case .urlAloneNews(let idNews):
        urlString = "https://ca.uatgootax.ru/v1/get_news/\(idNews)?current_time=423423322&city_id=26068"
    }

    AF.request(urlString, headers: self.headers).response { response in
      guard let data = response.data else { return }
      do {
        let decoder = JSONDecoder()
        let json = try decoder.decode(T.self, from: data)
        DispatchQueue.main.async {
          completion(json)
        }
      }
      catch {
        print("\(error)")
      }
    }
  }
}
