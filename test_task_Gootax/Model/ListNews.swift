import Foundation

// MARK: - Welcome
struct Welcome: Codable {
  let result: Result
}

// MARK: - WelcomeInfo
struct WelcomeInfo: Codable {
    let result: ResultNews
}

// MARK: - Result
struct Result: Codable {
  let list: [List]
}

// MARK: - ResultNews
struct ResultNews: Codable {
    let name, logo: String
    let resultDescription: String

    enum CodingKeys: String, CodingKey {
        case name, logo
        case resultDescription = "description"
    }
}

// MARK: - List
struct List: Codable {
  let id, activeFrom, name: String
  let logo: String

  enum CodingKeys: String, CodingKey {
    case id
    case activeFrom = "active_from"
    case name, logo
  }
}
