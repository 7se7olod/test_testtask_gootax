import UIKit
import SnapKit

class MenuCollectionViewCell: UICollectionViewCell {
  // MARK: - cell ID
  static let reusID = "MenuCollectionViewCell"

  // MARK: - property
  let menuImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = 10
    imageView.contentMode = .scaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()

  let nameMenuLabel: UILabel = {
    let label = UILabel()
    label.text = "Text menu"
    label.textAlignment = .center
    return label
  }()

  // MARK: - Initialization
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(menuImageView)
    menuImageView.snp.makeConstraints { make in
      make.top.left.right.equalToSuperview()
    }

    addSubview(nameMenuLabel)
    nameMenuLabel.snp.makeConstraints { make in
      make.bottom.right.left.equalToSuperview()
      make.top.equalTo(menuImageView.snp_bottomMargin).offset(5)
      make.height.equalTo(20)
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
