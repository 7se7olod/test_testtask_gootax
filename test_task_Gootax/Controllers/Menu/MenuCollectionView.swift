import UIKit

class MenuCollectionView: UICollectionView {

  // MARK: - Property
  private let menu = [
    UIImage(named: "img-1"),
    UIImage(named: "img-2"),
    UIImage(named: "img-3"),
    UIImage(named: "img-4"),
    UIImage(named: "img-4"),
    UIImage(named: "img-4")
  ]

  // MARK: - Initialization
  init() {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical
    super.init(frame: .zero, collectionViewLayout: layout)
    delegate = self
    dataSource = self
    backgroundColor = .white
    register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: MenuCollectionViewCell.reusID)
    showsVerticalScrollIndicator = false
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - UICollectionViewDataSource & UICollectionViewDelegate
extension MenuCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    menu.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard
      let cell = dequeueReusableCell(withReuseIdentifier: MenuCollectionViewCell.reusID, for: indexPath) as? MenuCollectionViewCell else { return UICollectionViewCell() }
    cell.menuImageView.image = menu[indexPath.row]
    return cell
  }
}

extension MenuCollectionView: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    let size = UIScreen.main.bounds.width / 4
    return CGSize(width: size, height: size)
  }
}
