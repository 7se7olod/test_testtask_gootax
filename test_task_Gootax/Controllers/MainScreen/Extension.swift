import UIKit

// MARK: - TableViewDelegate, TableViewDataSource
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
      case 2: return listNews.count
      default: break
    }
    return 1
  }

  // MARK: - number of sections
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }

  // MARK: - height for header in section
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    switch section {
      case 0: return 60
      case 1: return 30
      case 2: return 30
      default: break
    }
    return 40
  }

  // MARK: - view For Header in section
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = UITableViewHeaderFooterView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
    header.tintColor = .white
    
    switch section {
      case 0:
        mainButtonConstraints(header: header)
        adressButtonSetConstraints(header: header)
        return header
      case 1:
        header.textLabel?.text = "Меню"
        return header
      case 2:
        header.textLabel?.text = "Новости"
        return header
      default: return header
    }
  }

  // MARK: - height for row
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
      case 0:
        let screenWidth = UIScreen.main.bounds.width
        let width = screenWidth - screenWidth / 4
        let height = width * 0.35
        return height
      case 1:
        let screenWidth = UIScreen.main.bounds.width
        let height = screenWidth / 2
        return height + 10
      default: break
    }
    return 100
  }

  // MARK: - cell for row
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
      case 0:
        guard
          let bannersCell = tableView.dequeueReusableCell(
            withIdentifier: BannersCollectionTableViewCell.reuseID,
            for: indexPath) as? BannersCollectionTableViewCell else { return UITableViewCell() }

        bannersCell.addCollectionViewInCell()
        return bannersCell
      case 1:
        guard let menuCell = tableView.dequeueReusableCell(
          withIdentifier: MenuCollectionTableViewCell.reuseID,
          for: indexPath) as? MenuCollectionTableViewCell else { return UITableViewCell() }
        menuCell.addCollectionViewInCell()
        return menuCell
      case 2:
        guard
          let newsCell = tableView.dequeueReusableCell(
            withIdentifier: NewsTableViewCell.reuseID,
            for: indexPath) as? NewsTableViewCell else { return UITableViewCell() }

        let news = self.listNews[indexPath.row]
        newsCell.setData(urlImage: news.logo, dataText: news.id, description: news.name)
        newsCell.addConstraints()
        return newsCell
      default:
        break
    }
    return UITableViewCell()
  }

  // MARK: - did select row
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch indexPath.section {
      case 2:
        let detailNewsViewController = DetailNewsViewController()
        let newsId = listNews[indexPath.row].id
        networking.getNetworkNews(generalType: WelcomeInfo.self,
                                  requestType: .urlAloneNews(idNews: newsId)) { data in
          
          let logo = data.result.logo
          let name = data.result.name
          let resultDescription = data.result.resultDescription
          detailNewsViewController.updateInterface(image: logo,
                                               name: name,
                                               description: resultDescription)
        }
        present(detailNewsViewController, animated: true)
      default: break
    }
  }
}
