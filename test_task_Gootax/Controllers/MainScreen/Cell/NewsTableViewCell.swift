import UIKit
import SnapKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {

  // MARK: - Property
  static let reuseID = "NewsTableViewCell"
  private let foodImageView = UIImageView()
  private let dataLabel = UILabel()
  private let descriptionLabel = UILabel()

  // MARK: - Methods
  func setData(urlImage: String, dataText: String, description: String) {
    self.foodImageView.clipsToBounds = true
    self.foodImageView.layer.cornerRadius = 10
    guard let url = URL(string: urlImage) else { return }
    self.foodImageView.kf.setImage(with: url)
    self.dataLabel.text = dataText
    self.descriptionLabel.text = description
    self.descriptionLabel.minimumScaleFactor = 0.5
  }

  // MARK: - set constraints
  func addConstraints() {
    contentView.addSubview(foodImageView)
    foodImageView.snp.makeConstraints { make in
      make.top.equalToSuperview().inset(10)
      make.left.equalToSuperview().inset(10)
      make.bottom.equalToSuperview().inset(10)
      make.width.equalTo(120)
    }

    contentView.addSubview(dataLabel)
    dataLabel.snp.makeConstraints { make in
      make.top.equalToSuperview().inset(10)
      make.left.equalTo(foodImageView.snp_rightMargin).offset(20)
    }

    contentView.addSubview(descriptionLabel)
    descriptionLabel.snp.makeConstraints { make in
      make.left.equalTo(foodImageView.snp_rightMargin).offset(20)
      make.right.equalToSuperview().inset(20)
      make.top.equalToSuperview().offset(20)
      make.bottom.equalToSuperview().inset(10)
    }
  }
}
