import UIKit
import SnapKit

class BannersCollectionTableViewCell: UITableViewCell {
  // MARK: - Property
  static let reuseID = "BannersCollectionTableViewCell"
  private var bannerCollectionView = BannerCollectionView()

  // MARK: - Add constraints
  func addCollectionViewInCell() {
    contentView.addSubview(bannerCollectionView)
    bannerCollectionView.snp.makeConstraints { make in
      make.left.equalTo(self.contentView)
      make.right.equalTo(self.contentView)
      make.top.equalTo(self.contentView)
      make.bottom.equalTo(self.contentView)
    }
  }
}
