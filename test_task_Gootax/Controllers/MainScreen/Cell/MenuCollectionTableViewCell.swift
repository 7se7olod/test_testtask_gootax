import UIKit
import SnapKit

class MenuCollectionTableViewCell: UITableViewCell {
  // MARK: - Property
  static let reuseID = "MenuCollectionTableViewCell"
  private var menuCollectionView = MenuCollectionView()

  // MARK: - Add constraints
  func addCollectionViewInCell() {
    contentView.addSubview(menuCollectionView)
    menuCollectionView.snp.makeConstraints { make in
      make.left.equalTo(self.contentView).inset(20)
      make.right.equalTo(self.contentView).inset(20)
      make.top.equalTo(self.contentView)
      make.bottom.equalTo(self.contentView)
    }
  }
}
