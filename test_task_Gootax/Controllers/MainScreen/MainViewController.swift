import UIKit
import SnapKit

class MainViewController: UIViewController {

  // MARK: - Property
  private var tableView = UITableView()
  let networking = Networking()
  var listNews: [List] = []

  // MARK: - Buttons
  private var adressButton: UIButton = {
    let adressButton = UIButton()
    adressButton.setTitle(LocationAdress.location, for: .normal)
    adressButton.setTitleColor(UIColor.gray, for: .normal)
    adressButton.titleLabel?.adjustsFontSizeToFitWidth = true
    adressButton.titleLabel?.minimumScaleFactor = .leastNormalMagnitude
    adressButton.contentHorizontalAlignment = .left
    return adressButton
  }()

  private let mainButton: UIButton = {
    let mainButton = UIButton()
    mainButton.setImage(UIImage(systemName: "line.3.horizontal"), for: .normal)
    mainButton.tintColor = .orange
    return mainButton
  }()

  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    networking.getNetworkNews(generalType: Welcome.self, requestType: .urlListNews) { [weak self] result in
      guard let self = self else { return }
      DispatchQueue.main.async {
        self.listNews = result.result.list
        self.tableView.reloadData()
      }
    }
    configTableView()
  }

  // MARK: - Setup TableView
  private func configTableView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    registrationCells()
    setupConstraintsTableView()
  }

  // MARK: - registration cells
  private func registrationCells() {
    tableView.register(
      BannersCollectionTableViewCell.self,
      forCellReuseIdentifier:BannersCollectionTableViewCell.reuseID)
    tableView.register(
      NewsTableViewCell.self,
      forCellReuseIdentifier: NewsTableViewCell.reuseID)
    tableView.register(
      MenuCollectionTableViewCell.self,
      forCellReuseIdentifier: MenuCollectionTableViewCell.reuseID)
  }

  // MARK: - Setup constraints
  private func setupConstraintsTableView() {
    self.view.addSubview(tableView)
    tableView.snp.makeConstraints { make in
      make.left.right.bottom.equalToSuperview()
      make.topMargin.equalToSuperview().inset(20)
    }
  }

  func mainButtonConstraints(header: UITableViewHeaderFooterView) {
    header.addSubview(mainButton)
    mainButton.snp.makeConstraints { make in
      make.left.equalTo(header).inset(20)
      make.centerY.equalTo(header)
      make.width.height.equalTo(40)
    }
  }

  func adressButtonSetConstraints(header: UITableViewHeaderFooterView) {
    header.addSubview(adressButton)
    adressButton.addAction(actionAdressButton(), for: .touchUpInside)
    adressButton.snp.makeConstraints { make in
      make.left.equalTo(header).inset(70)
      make.right.equalTo(header).inset(40)
      make.top.bottom.equalTo(header).inset(20)
    }
  }

  // MARK: - Setup action adressButton
  private func actionButtonTapped() {
    let adressViewController = AdressTableViewController()
    present(adressViewController, animated: true)
  }

  private func actionAdressButton() -> UIAction {
    let adressActionButton = UIAction { [weak self] action in
      guard let self = self else { return }
      self.actionButtonTapped()
    }
    return adressActionButton
  }
}


