import UIKit
import SnapKit

class AdressTableViewCell: UITableViewCell {
  // MARK: - Property
  static let reuseID = "AdressTableViewCell"
  private let icon = UIImageView()
  private let nameAdressTextLabel = UILabel()
  private let adressTextLabel = UILabel()

  // MARK: - Methods
  func setData(image: UIImage, adressNameText: String, adressText: String) {
    self.icon.image = image
    self.nameAdressTextLabel.text = adressNameText
    self.adressTextLabel.text = adressText
  }

  // MARK: - Set constraints
  func addConstraints() {
    icon.tintColor = .gray
    contentView.addSubview(icon)
    icon.snp.makeConstraints { make in
      make.centerY.equalToSuperview()
      make.left.equalToSuperview().inset(20)
      make.height.equalTo(25)
      make.width.equalTo(30)
    }

    contentView.addSubview(nameAdressTextLabel)
    nameAdressTextLabel.snp.makeConstraints { make in
      make.top.equalToSuperview().inset(10)
      make.left.equalTo(icon.snp_rightMargin).offset(20)
    }

    adressTextLabel.textColor = .gray
    adressTextLabel.font = adressTextLabel.font.withSize(14)
    contentView.addSubview(adressTextLabel)
    adressTextLabel.snp.makeConstraints { make in
      make.left.equalTo(icon.snp_rightMargin).offset(20)
      make.right.equalToSuperview().inset(20)
      make.bottom.equalToSuperview().inset(5)
    }
  }
}
