import UIKit
import SnapKit

class AdressTableViewController: UITableViewController {

  // MARK: - Property
  private var adresses = [
    Adress(name: "Текущее местоположение",
           adress: LocationAdress.location,
           icon: UIImage(systemName: "location")!),
    Adress(name: "Дом",
           adress: "Ижевск, Ленина, 140",
           icon: UIImage(systemName: "house")!),
    Adress(name: "Работа",
           adress: "Ижевск, 10 лет Октября, 343",
           icon: UIImage(systemName: "bag")!)
  ]

  private let segment: UISegmentedControl = UISegmentedControl(items: ["Доставка", "Самовывоз", "В ресторане"])

  private let headerLabel: UILabel = {
    let label = UILabel()
    label.text = "Способ получения"
    label.textAlignment = .center
    return label
  }()

  private var saveButton: UIButton = {
    let button = UIButton()
    let y = UIScreen.main.bounds.maxY - 130
    let width = UIScreen.main.bounds.width - 40
    button.frame = CGRect(x: 20, y: y, width: width, height: 50)
    button.setTitle("Сохранить", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.backgroundColor = .orange
    button.layer.cornerRadius = 10
    return button
  }()

  private var addNewAdressButton: UIButton = {
    let button = UIButton()
    button.setTitle("Добавить адрес", for: .normal)
    button.setImage(UIImage(systemName: "plus"), for: .normal)
    button.tintColor = .orange
    button.contentHorizontalAlignment = .left
    button.setTitleColor( UIColor.black, for: .normal)
    button.titleEdgeInsets =  UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    return button
  }()

  // MARK: - LifeCylce
  override func viewDidLoad() {
    super.viewDidLoad()
    setupSegmentControl()
    tableView.separatorStyle = .none
    tableView.register(AdressTableViewCell.self, forCellReuseIdentifier: AdressTableViewCell.reuseID)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.tableView.addSubview(saveButton)
  }

  // MARK: - Setup segmented control
  private func setupSegmentControl() {
    navigationItem.titleView = segment
    segment.selectedSegmentIndex = 0
    segment.selectedSegmentTintColor = .orange
  }

  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return adresses.count
  }

  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: AdressTableViewCell.reuseID,
      for: indexPath) as? AdressTableViewCell else { return UITableViewCell() }

    let data = adresses[indexPath.row]
    if data.adress == LocationAdress.location {
      cell.setData(image: data.icon, adressNameText: data.name, adressText: data.adress)
      cell.addConstraints()
      cell.accessoryType = .checkmark
    }
    cell.setData(image: data.icon, adressNameText: data.name, adressText: data.adress)
    cell.addConstraints()
    cell.tintColor = .orange
    return cell
  }

  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))

    header.addSubview(headerLabel)
    headerLabel.snp.makeConstraints { make in
      make.top.equalTo(header)
      make.left.right.equalTo(header).inset(30)
    }

    header.addSubview(segment)
    segment.snp.makeConstraints { make in
      make.top.equalTo(headerLabel.snp_bottomMargin).offset(10)
      make.bottom.left.right.equalTo(header).inset(0)
    }
      return header
  }

  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))

    footerView.addSubview(addNewAdressButton)
    addNewAdressButton.snp.makeConstraints { make in
      make.left.right.equalTo(footerView).inset(20)
      make.bottom.top.equalTo(footerView).inset(10)
    }
    return footerView
  }

  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 60
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let cell = tableView.cellForRow(at: indexPath)
    let cells = tableView.visibleCells
    cells.forEach { $0.accessoryType = .none }
    cell?.accessoryType = .checkmark
  }
}
