import UIKit

class BannerCollectionView: UICollectionView {

  // MARK: - Property
  private var listNews: [List] = []
  private let networking = Networking()

  private let banners = [
    UIImage(named: "img-1"),
    UIImage(named: "img-2"),
    UIImage(named: "img-3"),
    UIImage(named: "img-4")
  ]

  // MARK: - Initialization
  init() {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    super.init(frame: .zero, collectionViewLayout: layout)
    delegate = self
    dataSource = self
    backgroundColor = .white
    register(BannerCollectionViewCell.self, forCellWithReuseIdentifier: BannerCollectionViewCell.reusID)
    showsHorizontalScrollIndicator = false
    getListNews()
  }

  // MARK: - get list news
  private func getListNews() {
    networking.getNetworkNews(generalType: Welcome.self, requestType: .urlListNews) { [weak self] result in
      guard let self = self else { return }
      DispatchQueue.main.async {
        self.listNews = result.result.list
        self.reloadData()
      }
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - UICollectionViewDataSource & UICollectionViewDelegate
extension BannerCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    banners.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard
      let cell = dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.reusID, for: indexPath) as? BannerCollectionViewCell else { return UICollectionViewCell() }
    cell.bannerImageView.image = banners[indexPath.row]
    return cell
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let detailNewsViewController = DetailNewsViewController()
    let newsId = listNews[indexPath.row].id
    networking.getNetworkNews(generalType: WelcomeInfo.self,
                              requestType: .urlAloneNews(idNews: newsId)) { data in

      let logo = data.result.logo
      let name = data.result.name
      let resultDescription = data.result.resultDescription
      detailNewsViewController.updateInterface(image: logo,
                                           name: name,
                                           description: resultDescription)
    }
    UIApplication.shared.keyWindow?.rootViewController?.present(detailNewsViewController, animated: true)
  }
}

// MARK: - CollectionViewDelegateFlowLayout
extension BannerCollectionView: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let screenWidth = UIScreen.main.bounds.width
    let width = screenWidth - screenWidth / 4
    let height = width * 0.3
    return CGSize(width: width, height: height)
  }
}
