import UIKit
import SnapKit

class BannerCollectionViewCell: UICollectionViewCell {
  // MARK: - cell ID
  static let reusID = "BannerCollectionViewCell"

  // MARK: - property
  let bannerImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = 10
    imageView.contentMode = .scaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()

  // MARK: - Initialization
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(bannerImageView)
    bannerImageView.snp.makeConstraints { make in
      make.top.left.right.bottom.equalToSuperview()
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
