import UIKit
import CoreLocation

class LaunchViewController: UIViewController {
  // MARK: - property
  private lazy var locationManager = CLLocationManager()

  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
  }

// MARK: - load main view controller
  private func loadMainView() {
    let mainVC = MainViewController()
    let window = UIWindow()
    window.rootViewController = mainVC
    UIApplication.shared.windows.first?.rootViewController = mainVC
    UIApplication.shared.windows.first?.makeKeyAndVisible()
    present(mainVC, animated: true)
  }
}


extension LaunchViewController: CLLocationManagerDelegate {
  // MARK: - geocoding
  private func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
    CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
  }

  // MARK: - CLLocationManagerDelegate
  func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
    switch locationManager.authorizationStatus {
      case .denied:
        self.loadMainView()
      case .authorizedWhenInUse:
        locationManager.startUpdatingLocation()
      default: break
    }
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let currentLocation = locations.last else { return }
    let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)

    geocode(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) { [weak self] placemark, error in
      guard
        let self = self,
        let placemark = placemark,
            error == nil else { return }

      let city = placemark.locality ?? ""
      let street = placemark.thoroughfare ?? ""
      let number = placemark.subThoroughfare ?? ""
      LocationAdress.location = "\(city), \(street), \(number)"
      self.loadMainView()
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print(error.localizedDescription)
  }
}
