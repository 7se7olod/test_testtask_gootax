import UIKit
import SnapKit
import Kingfisher

class DetailNewsViewController: UIViewController {
  // MARK: - Property
  let newsImageView = UIImageView()
  let nameNewsLabel = UILabel()
  let descrtiptionNewLabel = UILabel()

  var name = ""
  var descriptionNews = ""
  var image = ""

  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    initialization()
    updateInterface(image: image, name: name, description: descriptionNews)
  }

  // MARK: - Methods
  func updateInterface(image: String, name: String, description: String) {
    DispatchQueue.main.async {
      self.descrtiptionNewLabel.textAlignment = .center
      self.descrtiptionNewLabel.minimumScaleFactor = 1
      guard let url = URL(string: image) else { return }
      self.newsImageView.kf.setImage(with: url)
      self.nameNewsLabel.text = name
      self.descrtiptionNewLabel.text = description
    }
  }
  
  // MARK: - Initialization
  func initialization() {
    newsImageView.clipsToBounds = true
    newsImageView.layer.cornerRadius = 10
    self.view.addSubview(newsImageView)
    newsImageView.snp.makeConstraints { make in
      make.left.right.top.equalToSuperview().inset(20)
      make.height.equalTo(300)
    }

    self.view.addSubview(nameNewsLabel)
    nameNewsLabel.snp.makeConstraints { make in
      make.top.equalTo(newsImageView.snp_bottomMargin).offset(30)
      make.centerX.equalToSuperview()
      make.left.right.equalToSuperview().inset(20)
    }

    descrtiptionNewLabel.numberOfLines = 0
    self.view.addSubview(descrtiptionNewLabel)
    descrtiptionNewLabel.snp.makeConstraints { maker in
      maker.top.equalTo(nameNewsLabel.snp_bottomMargin).offset(20)
      maker.left.right.equalToSuperview().inset(20)
      maker.bottom.equalToSuperview().inset(40)
    }
  }
}
